This repository is for the ESS Main Control Room Operations to store and update layouts saved and shared using Control System Studio Phoebus for Neutron Source Operations.

# Naming convention

Layouts are prefixed by either: 
- layout_ : Meant for layouts covering all screens, to control a full system
	- In Phoebus, load one of these with Window -> Load Layout
or
- window_ : Meant for individual extra windows
	- In Phoebus, load one of these with Window -> Add Layout


